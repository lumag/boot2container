# Sourcing the initscript tends to destroy the shunit environment.
# To make running tests easier, run them in a subshell and forward
# the important environment variables back
run_unit_test() {
    (
        export UNITTEST=1
        source ./initscript.sh

        log() {
            :
        }

        $@
        ret=$?

        # For some reason, I can't seem to be able to list these variables...
        echo "__shunit_testSuccess=$__shunit_testSuccess
              __shunit_assertsTotal=${__shunit_assertsTotal}
              __shunit_assertsPassed=${__shunit_assertsPassed}
              __shunit_assertsFailed=${__shunit_assertsFailed}
              __shunit_assertsSkipped=${__shunit_assertsSkipped}" > /tmp/test_env

        return $ret
    )

    source /tmp/test_env
    rm /tmp/test_env
}

start_subtest() {
    echo -e "\n  > $@"
}
